package oleg.zinevich;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.lang.reflect.Field;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by oleg.zinevich on 03/08/2017.
 */
public class HttpDownloaderImplTest {


    private HttpDownloader downloader;

    @Before
    public void runBeforeTestMethod() throws Exception {
        try {
            downloader = new HttpDownloaderImpl();
            Map<String, Long> blockedHosts = new HashMap<String, Long>();
            blockedHosts.put("icdn.lenta.ru", System.currentTimeMillis());
            Field field = HttpDownloaderImpl.class.getDeclaredField("blockedHosts");
            field.setAccessible(true);
            field.set(null, blockedHosts);
        }
        catch(Exception e) {
            e.printStackTrace();
            Assert.fail();
        }

    }

    @Test(expected = HostBlockedException.class)
    public void testGetException() throws Exception {
            downloader.get(new URL("https://icdn.lenta.ru/images/2017/08/03/17/20170803170943944/top7_d33ec92a14b0c2749226193d2bdf6b0a.jpg"),
                    new File("C:/test.jpg"));
    }

    @Test
    public void testGetWait() throws Exception {
        try {
            downloader.get(new URL("https://icdn.lenta.ru/images/2017/08/03/17/20170803170943944/top7_d33ec92a14b0c2749226193d2bdf6b0a.jpg"),
                    new File("C:/test.jpg"));
        }
        catch(HostBlockedException e) {
            synchronized (HttpDownloaderImplTest.class) {
                HttpDownloaderImplTest.class.wait(11000);
            }
        }
        downloader.get(new URL("https://icdn.lenta.ru/images/2017/08/03/17/20170803170943944/top7_d33ec92a14b0c2749226193d2bdf6b0a.jpg"),
                    new File("C:/test.jpg"));

    }

}