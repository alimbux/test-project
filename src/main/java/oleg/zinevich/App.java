package oleg.zinevich;

        import java.io.File;
        import java.net.URL;
        import java.util.Scanner;

/**
 * Hello world!
 *
 */
public class App
{
    public static void main( String[] args )
    {
        Scanner scanner = new Scanner(System.in);
        HttpDownloader downloader = new HttpDownloaderImpl();

        while (true) {

            System.out.println("Enter host and path to file: ");
            String input = scanner.nextLine();

            if ("q".equals(input)) {
                break;
            }
            try {
                input = input.trim();
                args = input.split(" ");
                if (args.length >= 2) {
                    downloader.get(new URL(args[0]), new File(args[1]));
                }
                System.out.println("File successfully downloaded");
            }
            catch(Exception e) {
                System.out.println("error : " + e.getMessage());
            }

        }

        scanner.close();

    }
}
