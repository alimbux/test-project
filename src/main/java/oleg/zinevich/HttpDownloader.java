package oleg.zinevich;

import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * Created by oleg.zinevich on 03/08/2017.
 */
public interface HttpDownloader {
    /*
* This method downloads the file specified by URL from the remote host
* and stores it in File of local file system.
*/
    void get(URL url, File localFile) throws IOException, HostBlockedException;
}
