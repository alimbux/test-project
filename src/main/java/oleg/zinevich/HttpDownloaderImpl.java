package oleg.zinevich;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by oleg.zinevich on 03/08/2017.
 */
public class HttpDownloaderImpl implements HttpDownloader {

    //TODO make configurable
    private static long timeToBlock = 10000;

    private static Map<String, Long> blockedHosts = new HashMap<String, Long>();

    public void get(URL url, File localFile) throws IOException, HostBlockedException {
        try {
            if (isBlocked(url.getHost())) {
                throw new HostBlockedException("host " + url.getHost() + " is blocked");
            }
            FileUtils.copyURLToFile(url, localFile, 10000, 10000);

        } catch (IOException e) {
            blockHost(url.getHost());
            //TODO add logging
            e.printStackTrace();
            throw new IOException();
        }
    }

    protected synchronized static void blockHost(String host) {
        blockedHosts.put(host, System.currentTimeMillis());
    }

    protected synchronized static boolean isBlocked(String host) {
        if (blockedHosts.get(host) != null) {
            if (blockedHosts.get(host) + timeToBlock > System.currentTimeMillis()) {
                return true;
            } else {
                blockedHosts.remove(host);
            }
        }
        return false;
    }
}
