package oleg.zinevich;

/**
 * Created by oleg.zinevich on 03/08/2017.
 */
public class HostBlockedException extends Exception {

    HostBlockedException(String msg) {
        super(msg);
    }
}
